//
//  NSObject+Observer.m
//  ResponseStyle
//
//  Created by CavanSu on 17/7/5.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import "NSObject+Observer.h"
#import "objc/message.h"
#import "SubPerson.h"

static NSString *relationKeyWord = @"keyWord";

@implementation NSObject (Observer)

- (void)cav_addObserver:( NSObject * _Nonnull )observer forKeyPath:( NSString * _Nonnull)keyPath options:(NSKeyValueObservingOptions)options context:(nullable void *)context {
    
    /*
     创建关联要使用到Objective-C的运行时函数：objc_setAssociatedObject来把一个对象与另外一个对象进行关联。该函数需要四个参数：源对象，关键字，关联的对象和一个关联策略。当然，此处的关键字和关联策略是需要进一步讨论的。
     ■  关键字是一个void类型的指针。每一个关联的关键字必须是唯一的。通常都是会采用静态变量来作为关键字。
     ■  关联策略表明了相关的对象是通过赋值，保留引用还是复制的方式进行关联的；还有这种关联是原子的还是非原子的。这里的关联策略和声明属性时的很类似。这种关联策略是通过使用预先定义好的常量来表示的。
     
     */
    
    // 该方法将 self 与 观察者observer 进行关联（或者说存储）
    objc_setAssociatedObject(self, (__bridge const void *)(relationKeyWord), observer, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    // isa指针 指向 子类
    object_setClass(self, [SubPerson class]);
}

@end
