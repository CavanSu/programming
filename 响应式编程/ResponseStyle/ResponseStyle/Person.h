//
//  Person.h
//  ResponseStyle
//
//  Created by CavanSu on 17/7/5.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
@property (nonatomic, copy) NSString *name;
@end
