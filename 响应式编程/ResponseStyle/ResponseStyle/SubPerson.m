//
//  SubPerson.m
//  ResponseStyle
//
//  Created by CavanSu on 17/7/5.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import "SubPerson.h"
#import "objc/message.h"

static NSString *relationKeyWord = @"keyWord";

@implementation SubPerson

- (void)setName:(NSString *)name {
    [super setName:name];
    
    //获取观察者对象
    id objc = objc_getAssociatedObject(self, (__bridge const void *)(relationKeyWord));
    //通知外界
    [objc observeValueForKeyPath:@"name" ofObject:self change:nil context:nil];
}

@end
