//
//  NSObject+Observer.h
//  ResponseStyle
//
//  Created by CavanSu on 17/7/5.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Observer)

- (void)cav_addObserver:( NSObject * _Nonnull )observer forKeyPath:( NSString * _Nonnull)keyPath options:(NSKeyValueObservingOptions)options context:(nullable void *)context;

@end
