//
//  Calculation.swift
//  LinkStyle
//
//  Created by CavanSu on 2019/5/8.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import Cocoa

typealias CalcuBlock = ((Int) -> Calculation)

class Calculation: NSObject {
    var result: Int = 0
    
    var add: ((Int) -> Calculation) {
        let block: CalcuBlock = { [unowned self] (number: Int) -> Calculation in
            self.result += number
            return self
        }
        return block
    }
    
    static func calculate(block: ((_ make: Calculation) -> Void)) -> Int {
        let instance = Calculation()
        block(instance)
        return instance.result
    }
    
    func subtraction() -> ((Int) -> Calculation) {
        let block: CalcuBlock = { [unowned self] (number: Int) -> Calculation in
            self.result -= number
            return self
        }
        return block
    }
}
