//
//  main.swift
//  LinkStyle
//
//  Created by CavanSu on 2019/5/8.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import Foundation

let result = Calculation.calculate { (make) in
    _ = make.add(5).subtraction()(1)
}

print("result: \(result)")
