//
//  Calculation.h
//  LinkStyle
//
//  Created by CavanSu on 17/7/5.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
#define isProperty 0

@class Calculation;
typedef Calculation *(^addBlock)(int);
typedef int(^test)();

@interface Calculation : NSObject

#if isProperty
@property (nonatomic, copy) addBlock addition;
@property (nonatomic, copy) Calculation *(^subtraction)(int);
@property (nonatomic, copy) Calculation *(^multiplication)(int);
@property (nonatomic, copy) Calculation *(^division)(int);
#else
- (Calculation* (^)(int))addition;
- (Calculation* (^)(int))subtraction;
- (Calculation* (^)(int))multiplication;
- (Calculation* (^)(int))division;
#endif

+ (int)calculate:(void (^)(Calculation *make)) code;
@end
