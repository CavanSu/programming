//
//  ViewController.m
//  LinkStyle
//
//  Created by CavanSu on 17/7/5.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import "ViewController.h"
#import "Calculation.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    NSInteger result =  [Calculation calculate:^(Calculation *make) {
        // make.addition 返回一个block， 然后 (make.addition)(4) 调用这个 block
        (make.addition)(4);
        make.addition(4).division(1).multiplication(5).division(2);
    }];
    
    NSLog(@"%zd", result);
    
    [Calculation calculate:^(Calculation *make) {
        
    }];
}

@end
