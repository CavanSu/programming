//
//  Calculation.m
//  LinkStyle
//
//  Created by CavanSu on 17/7/5.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import "Calculation.h"

@interface Calculation()
@property (nonatomic, assign) float result;
@end

@implementation Calculation

+ (int)makeCount:(void (^)(Calculation *make)) code {
    Calculation *countManger = [[Calculation alloc] init];
    code(countManger);

    return countManger.result;
}

- (addBlock)addition {
    return ^(int number) {
        self.result += number;
        return self;
    };
}

- (Calculation *(^)(int))subtraction {
    return ^(int number) {
        self.result -= number;
        return self;
    };
}

- (Calculation *(^)(int))multiplication {
    return ^(int number) {
        self.result *= number;
        return self;
    };
}

- (Calculation *(^)(int))division {
    return ^(int number) {
        self.result /= number;
        return self;
    };
}

@end
